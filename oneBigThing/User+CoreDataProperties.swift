//
//  User+CoreDataProperties.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/11/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var userID: NSNumber?
    @NSManaged var userName: String?
    @NSManaged var firstName: String?
    @NSManaged var lastName: String?
    @NSManaged var messages: NSSet?

}
