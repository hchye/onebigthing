//
//  MessageTableViewCell.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/11/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
//MARK: Outlets
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var fullNameLabel: UILabel!
    
    @IBOutlet weak var messageTextLabel: UILabel!
    
//    @IBOutlet weak var messageTextView: UITextView!

    
    //MARK: methods
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
